/*-------------------------------*/
#include "setup.h"
#include "define.h"
#include "delay.h"
#include "lcd.h"
#include <stdbool.h>

/*-------------------------------*/
extern volatile uint8_t g_RxMode, g_FlagRx;
/*-------------------------------*/
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
/*-------------------------------*/
extern uint8_t g_SelectMode, g_StepMode;
extern uint8_t g_Stop;
extern uint8_t g_Blink;


static void btn_ent(void);
static void btn_up(void);
static void btn_down(void);

static void btn_ent_select_mode(void);
static void btn_up_select_mode(void);
static void btn_down_select_mode(void);

static void btn_ent_no_mode(void);
static void btn_up_no_mode(void);
static void btn_down_no_mode(void);

static void btn_ent_testIo_mode(void);
static void btn_up_testIo_mode(void);
static void btn_down_testIo_mode(void);

static void btn_ent_calib_mode(void);
static void btn_up_calib_mode(void);
static void btn_down_calib_mode(void);

static void btn_ent_processR_mode(void);
static void btn_up_processR_mode(void);
static void btn_down_processR_mode(void);

static void btn_ent_debug_mode(void);
static void btn_up_debug_mode(void);
static void btn_down_debug_mode(void);

static void btn_stop(void);
/* for display */
static void show_menu(void);
static void _disPlay_SelectMode(uint8_t);
static void _disPlay_NoMode(uint8_t);
static void _disPlay_TestIoMode(uint8_t);
static void _disPlay_CalibMode(uint8_t);
static void _disPlay_ProcessMode(uint8_t);
static void _disPlay_DebugMode(uint8_t);


void SETUP_btn_process(uint16_t GPIO_Pin)
{
	uint8_t count_button = 0;
	// Init GPIO INPUT
	GPIO_InitTypeDef GPIO_Init;
	GPIO_Init.Pin = GPIO_Pin;
	GPIO_Init.Mode = GPIO_MODE_INPUT;
	GPIO_Init.Pull = GPIO_PULLUP;
  	HAL_GPIO_Init(GPIOB, &GPIO_Init);

	for(uint8_t i = 0; i < 10; i++)
	{
		if(HAL_GPIO_ReadPin(GPIOB, GPIO_Pin) == GPIO_PIN_RESET)
		{
			 count_button++;
			 DELAY(10);
		}
		else
		{
			count_button = 0;
		}
	}

	if(count_button == 10)
	{
		if(GPIO_Pin == GPIO_PIN_3)
		{
			btn_ent();
		}

		else if(GPIO_Pin == GPIO_PIN_5)
		{
			btn_down();
		}
		else if(GPIO_Pin == GPIO_PIN_7)
		{
			btn_up();
		}	
	}

	// Init GPIO EX IT
	GPIO_Init.Pin = GPIO_Pin;
	GPIO_Init.Mode = GPIO_MODE_IT_FALLING;
	GPIO_Init.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOB, &GPIO_Init);
}



char Line1[16];
char Line2[16];
uint8_t s_ControlState = NONE;
uint8_t s_SetupSate = NONE;
uint8_t s_Mode = NO_MODE;
uint8_t s_ModeStatus;
bool s_StateCur = false, s_StateOld = false;


static void btn_ent(void)
{
	if(g_SelectMode == PROCESS_R || g_SelectMode == DEBUG_R) btn_stop();
	else
	{
		if(s_ControlState == NONE)
		{ 
			s_ControlState = SELECT_MODE;
			g_Blink = 1;
			show_menu();
		}

		else if(s_ControlState == SELECT_MODE)
		{
			g_Blink = 0;
			btn_ent_select_mode();
			_disPlay_SelectMode(BTN_ENT);
		}
		else if(s_ControlState == NO_MODE_ENT)
		{
			btn_ent_no_mode();
			_disPlay_NoMode(BTN_ENT);
		}
		else if(s_ControlState == MODE_TEST_IO_ENT)
		{
			btn_ent_testIo_mode();
			_disPlay_TestIoMode(BTN_ENT);
		}
		else if(s_ControlState == MODE_CALIB_ENT)
		{
			btn_ent_calib_mode();
			_disPlay_CalibMode(BTN_ENT);
		}
		else if(s_ControlState == MODE_PROCESS_R_ENT)
		{
			btn_ent_processR_mode();
			_disPlay_ProcessMode(BTN_ENT);
		}
		else if(s_ControlState == MODE_DEBUG_ENT)
		{
			btn_ent_debug_mode();
			_disPlay_DebugMode(BTN_ENT);
		}
	}
}

static void btn_up(void)
{
	if(s_ControlState== NONE)
	{

	}
	
	else if(s_ControlState == SELECT_MODE)
	{
		btn_up_select_mode();
		_disPlay_SelectMode(BTN_UP);
	}
	else if(s_ControlState == NO_MODE_ENT)
	{
		btn_up_no_mode();
		_disPlay_NoMode(BTN_UP);
	}
	else if(s_ControlState == MODE_TEST_IO_ENT)
	{
		btn_up_testIo_mode();
		_disPlay_TestIoMode(BTN_UP);
	}
	else if(s_ControlState == MODE_CALIB_ENT)
	{
		btn_up_calib_mode();
		_disPlay_CalibMode(BTN_UP);
	}
	else if(s_ControlState == MODE_PROCESS_R_ENT)
	{
		btn_up_processR_mode();
		_disPlay_ProcessMode(BTN_UP);
	}
	else if(s_ControlState == MODE_DEBUG_ENT)
	{
		btn_up_debug_mode();
		_disPlay_DebugMode(BTN_UP);
	}
}

static void btn_down(void)
{
	if(s_ControlState == NONE)
	{

	}
	
	else if(s_ControlState == SELECT_MODE)
	{
		btn_down_select_mode();
		_disPlay_SelectMode(BTN_DOWN);
	}
	else if(s_ControlState == NO_MODE_ENT)
	{
		btn_down_no_mode();
		_disPlay_NoMode(BTN_DOWN);
	}
	else if(s_ControlState == MODE_TEST_IO_ENT)
	{
		btn_down_testIo_mode();
		_disPlay_TestIoMode(BTN_DOWN);
	}
	else if(s_ControlState == MODE_CALIB_ENT)
	{
		btn_down_calib_mode();
		_disPlay_CalibMode(BTN_DOWN);
	}
	else if(s_ControlState == MODE_PROCESS_R_ENT)
	{
		btn_down_processR_mode();
		_disPlay_ProcessMode(BTN_DOWN);
	}
	else if(s_ControlState == MODE_DEBUG_ENT)
	{
		btn_down_debug_mode();
		_disPlay_DebugMode(BTN_DOWN);
	}
}


static void btn_stop(void)
{
	g_Stop = STOP_EN;
	s_ControlState = g_SelectMode = NONE;
}

static void btn_ent_select_mode(void)
{
	if(s_Mode == NO_MODE)        s_ControlState = s_SetupSate = NO_MODE_ENT;
	if(s_Mode == MODE_TEST_IO)   s_ControlState = s_SetupSate = MODE_TEST_IO_ENT;
	if(s_Mode == MODE_CALIB)     s_ControlState = s_SetupSate = MODE_CALIB_ENT;
	if(s_Mode == MODE_PROCESS_R) s_ControlState = s_SetupSate = MODE_PROCESS_R_ENT;
	if(s_Mode == MODE_DEBUG)     s_ControlState = s_SetupSate = MODE_DEBUG_ENT;
}

static void btn_up_select_mode(void)
{
	s_Mode++;
	if(s_Mode > MODE_DEBUG) s_Mode = NO_MODE;
}

static void btn_down_select_mode(void)
{
	s_Mode--;
	if(s_Mode < NO_MODE) s_Mode = MODE_DEBUG;
}

static void btn_ent_no_mode(void)
{
	
}

static void btn_up_no_mode(void)
{
	
}

static void btn_down_no_mode(void)
{

}

static void btn_ent_testIo_mode(void)
{

}

static void btn_up_testIo_mode(void)
{

}

static void btn_down_testIo_mode(void)
{

}

static void btn_ent_calib_mode(void)
{

}

static void btn_up_calib_mode(void)
{

}

static void btn_down_calib_mode(void)
{

}

static void btn_ent_processR_mode(void)
{
	g_SelectMode = PROCESS_R;
}

static void btn_up_processR_mode(void)
{

}

static void btn_down_processR_mode(void)
{

}

static void btn_ent_debug_mode(void)
{
	g_SelectMode = DEBUG_R;
}

static void btn_up_debug_mode(void)
{

}

static void btn_down_debug_mode(void)
{

}

/*----------------------------------*/
static void show_menu(void)
{
	sprintf(Line1, "  Select  mode  ");
	switch(s_Mode)
	{
		case NO_MODE: 			sprintf(Line2, "    nothing     "); break;
		case MODE_TEST_IO: 		sprintf(Line2, "    test io     "); break;
		case MODE_CALIB: 		sprintf(Line2, "    calib       "); break;
		case MODE_PROCESS_R:	sprintf(Line2, "    calculate R "); break;
		case MODE_DEBUG: 		sprintf(Line2, "    send to PC  "); break;
		default: break;
	}
	LCD_Puts(1, 1, Line1);
	LCD_Puts(2, 1, Line2);
}

static void _disPlay_SelectMode(uint8_t x)
{
	sprintf(Line1, "  Select  mode  ");
	if(x == BTN_ENT) 
	{
		switch(s_Mode)
		{
			case NO_MODE: 			sprintf(Line1, "  do nothing    "); break;
			case MODE_TEST_IO: 		sprintf(Line1, "  test io mode  "); break;
			case MODE_CALIB: 		sprintf(Line1, "  calib mode    "); break;
			case MODE_PROCESS_R:	sprintf(Line1, "  calculate mode"); break;
			case MODE_DEBUG: 		sprintf(Line1, "  send data mode"); break;
			default: break;
		}
		sprintf(Line2, "! enter to run !");
	}
	else
	{
		switch(s_Mode)
		{
			case NO_MODE: 			sprintf(Line2, "    nothing     "); break;
			case MODE_TEST_IO: 		sprintf(Line2, "    test io     "); break;
			case MODE_CALIB: 		sprintf(Line2, "     calib      "); break;
			case MODE_PROCESS_R:	sprintf(Line2, "  calculate R   "); break;
			case MODE_DEBUG: 		sprintf(Line2, "  send to PC    "); break;
			default: break;
		}
	}
	LCD_Puts(1, 1, Line1);
	LCD_Puts(2, 1, Line2);
}

static void _disPlay_NoMode(uint8_t x)
{

}

static void _disPlay_TestIoMode(uint8_t x)
{

}

static void _disPlay_CalibMode(uint8_t x)
{

}

static void _disPlay_ProcessMode(uint8_t x)
{
	
}

static void _disPlay_DebugMode(uint8_t x)
{

}



