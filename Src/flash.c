/*---------------------------------------------*/
#include "flash.h"

void FLASH_write(uint32_t add, uint8_t *data, uint8_t length)
{
	uint32_t len;
	uint16_t temp;
	uint32_t pageError = 0;
	FLASH_EraseInitTypeDef EraseInit = { FLASH_TYPEERASE_PAGES, add, 1 };
	HAL_FLASH_Unlock();
	HAL_FLASHEx_Erase(&EraseInit, &pageError);
	for(len = 0; len < length; len+=2) 
	{
		temp = (((uint16_t)data[len])<<8) | data[len+1];
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, add+2*len, temp);
	}
	HAL_FLASH_Lock();
}

void FLASH_read(uint32_t add, uint8_t *data, uint8_t length)
{
	uint32_t len;
	uint16_t temp;
  for(len = 0; len <= length; len+=2) 
	{
		temp = *(uint32_t*)(add+2*len);
		data[len] = temp >> 8;
		data[len+1] = temp;
	}
}

void FLASH_writeParams(uint32_t add, uint8_t *data1, uint8_t *data2, uint8_t length)
{
	uint32_t len;
	uint16_t temp;
	uint32_t pageError = 0;
	FLASH_EraseInitTypeDef EraseInit = { FLASH_TYPEERASE_PAGES, add, 1 };
	HAL_FLASH_Unlock();
	HAL_FLASHEx_Erase(&EraseInit, &pageError);
	for(len = 0; len < length; len+=2) 
	{
		temp = (((uint16_t)data1[len])<<8) | data1[len+1];
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, add+2*len, temp);
	}
	
	for(len = 0; len < length; len+=2) 
	{
		temp = (((uint16_t)data2[len])<<8) | data2[len+1];
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, add+30+2*len, temp);
	}
	HAL_FLASH_Lock();
}

void FLASH_readParams(uint32_t add, uint8_t *data1, uint8_t *data2, uint8_t length)
{
	uint32_t len;
	uint16_t temp;
  for(len = 0; len <= length; len+=2) 
	{
		temp = *(uint32_t*)(add+2*len);
		data1[len] = temp >> 8;
		data1[len+1] = temp;
	}
	
	 for(len = 0; len <= length; len+=2) 
	{
		temp = *(uint32_t*)(add+30+2*len);
		data2[len] = temp >> 8;
		data2[len+1] = temp;
	}
}


void FLASH_erase(uint32_t add)
{
	uint32_t pageError = 0;
	FLASH_EraseInitTypeDef EraseInit = { FLASH_TYPEERASE_PAGES, add, 1 };
	HAL_FLASH_Unlock();
	HAL_FLASHEx_Erase(&EraseInit, &pageError);
	HAL_FLASH_Lock();
}
