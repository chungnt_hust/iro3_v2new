/*-------------------------*/
#include "AD8402.h"
#include "define.h"

#define CS_1k(value) 		HAL_GPIO_WritePin(CS_1k_GPIO_Port,   CS_1k_Pin,   (value)?GPIO_PIN_SET:GPIO_PIN_RESET)
#define CS_100k(value)  HAL_GPIO_WritePin(CS_100k_GPIO_Port, CS_100k_Pin, (value)?GPIO_PIN_SET:GPIO_PIN_RESET)
#define CLK_POT(value)	HAL_GPIO_WritePin(CLK_POT_GPIO_Port, CLK_POT_Pin, (value)?GPIO_PIN_SET:GPIO_PIN_RESET)
#define SI_POT(value)   HAL_GPIO_WritePin(SPI_POT_GPIO_Port, SPI_POT_Pin, (value)?GPIO_PIN_SET:GPIO_PIN_RESET)

enum status { reset = 0, set };

void AD8402_writeData(uint8_t ver, uint8_t channel, uint8_t byte)
{
	CS_1k(set);
	CS_100k(set);
	CLK_POT(reset);
	
	uint8_t cnt;
	uint16_t temp;
	temp = (channel << 8) | byte;
	
	if(ver == ver100k)
	{
		CS_1k(reset);
		for(cnt = 0; cnt < 10; cnt++)
		{
			CLK_POT(reset);
			(temp&0x0200) ? SI_POT(set) : SI_POT(reset);		
			CLK_POT(set);
			temp <<= 1;
		}
		CLK_POT(reset);
		CS_1k(set);		
	}
	
	else
	{
		CS_100k(reset);
		for(cnt = 0; cnt < 10; cnt++)
		{
			CLK_POT(reset);
			(temp&0x0200) ? SI_POT(set) : SI_POT(reset);		
			CLK_POT(set);
			temp <<= 1;
		}
		CLK_POT(reset);
		CS_100k(set);
	}
}

