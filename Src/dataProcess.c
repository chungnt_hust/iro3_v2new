/*-------------------------*/
#include "dataProcess.h"
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

extern UART_HandleTypeDef huart2;
/*-------- RX type -----------------------*/
extern volatile uint8_t g_RxBuffer[150];
extern volatile uint8_t g_RxByte;
extern volatile uint8_t g_RxCount;
/*-------------------------------*/

static void DATAPROCESS_tachDau(rxType_t *rx)
{
	uint8_t i;
	rx->CNT = -1;
	for(i = 0; i < g_RxCount; i++)
	{
		if(g_RxBuffer[i] == '[' || g_RxBuffer[i] == ',' || g_RxBuffer[i] == ']')
		{		
			rx->CNT++;
			rx->LVT[rx->CNT] = i;							
		}
	}
}

/* destination : dich den */
static void DATAPROCESS_tachChuoi(uint8_t *des, volatile uint8_t *src, uint8_t begin, uint8_t length)
{
	memcpy(des, (const char*)(src+begin), length);
	des[length] = '\0';
}

/* tach tu mang rx buffer cac gia tri luu vao value */
void DATAPROCESS_tachGiatri(rxType_t *rx)
{
	uint8_t cnt;
	uint8_t temp[10];
	DATAPROCESS_tachDau(rx);
	for(cnt = 0; cnt < rx->CNT ; cnt++)
	{
		DATAPROCESS_tachChuoi(temp, g_RxBuffer, rx->LVT[cnt] + 1, rx->LVT[cnt+1] - rx->LVT[cnt] - 1);
		rx->VALUE[cnt] = atoi((const char*)temp);
	}
}


