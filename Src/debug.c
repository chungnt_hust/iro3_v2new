
#include "debug.h"
#include "define.h"
#include <string.h>

extern UART_HandleTypeDef huart2;
extern uint8_t g_vArr100k[25] , g_vArr1k[25];
extern uint8_t g_SelectMode, g_Stop;

void DEBUG_sendValueR(void)
{
		char sentStr[100] = "100k: {";
		for(uint8_t i = 0; i <  21; i++)
		{
			if(g_Stop == STOP_EN) { g_SelectMode = NONE; break; }
			char tmp[10];
			sprintf(tmp, "%d,", g_vArr100k[i]);
			strcat(sentStr,tmp);
		}
		strcat(sentStr, "}\r\n");
		HAL_UART_Transmit(&huart2, (uint8_t*)sentStr, strlen(sentStr), 500);
		
		strcpy(sentStr, "1k: {");	
		for(uint8_t i = 0; i <  21; i++)
		{
			if(g_Stop == STOP_EN) { g_SelectMode = NONE; break; }
			char tmp[10];
			sprintf(tmp, "%d,", g_vArr1k[i]);
			strcat(sentStr, tmp);
		}
		strcat(sentStr, "}\r\n");
		HAL_UART_Transmit(&huart2, (uint8_t*)sentStr, strlen(sentStr), 500);
		if(g_Stop == STOP_EN) g_Stop =  STOP_DIS;
}
