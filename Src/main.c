
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include <string.h>
#include "lcd.h"
#include "dataProcess.h"
#include "define.h"
#include "calculateR.h"
#include "AD8402.h"
#include "testIo.h"
#include "flash.h"
#include "debug.h"
#include "delay.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/



/*-------- RX type -----------------------*/
volatile uint8_t g_RxBuffer[150];
volatile uint8_t g_RxByte;
volatile uint8_t g_RxCount;
/*----------Flag receive mode---------------------*/
volatile uint8_t g_RxMode, g_FlagRx;
/*----------ADC table, TDS table, DEBUG table type---------------------*/
rxType_t g_AdcTable, g_TdsTable, g_DebugTable, g_TestIo;
/*----------Array Value 100k, 1k---------------------*/
//uint8_t g_vArr100k[25] = {10,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}
//		  , g_vArr1k[25] = {1,20,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};

uint8_t g_vArr100k[25] , g_vArr1k[25];
/*----------select mode---------------------*/
uint8_t g_SelectMode = NONE, g_StepMode = NONE, g_CalibEn = NONE;
/*------------------------------------------*/
uint8_t s_HasAdc, s_HasTds, s_HasDebug, s_HasRvalue;
/*------------------------------------------*/
uint8_t g_Stop;
/*------------------------------------------*/										
bool g_Flag1s;
											
//100k: {0,0,45, 36, 30, 23, 19, 15, 13, 11, 9,  4,  2,  2,  1,  1,  1,  1,  1,  1,  0,}
//1k:   {0,0,217,170,198,187,170,204,204,216,171,198,243,174,212,176,169,168,168,167,157,}
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */

	//#define TEST_EN
	R_init();
	HAL_UART_Receive_IT(&huart3, (uint8_t *)&g_RxByte, 1);
	HAL_TIM_Base_Start_IT(&htim2);
	
	LCD_Init();
	LCD_Puts(1, 1, "______IRO3______");
	LCD_Puts(2, 1, "----------------"); 

	//FLASH_writeParams(ADDR_FLASH_PAGE_30, g_vArr100k, g_vArr1k, 25);
	FLASH_readParams(ADDR_FLASH_PAGE_30, g_vArr100k, g_vArr1k, 25);
	AD8402_writeData(ver100k, IN_CHANNEL, 255);
	//g_SelectMode = PROCESS_R;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
		#ifndef TEST_EN
		if(g_SelectMode == PROCESS_R)
		{
			R_getAdcTable(OUT_CHANNEL);
			R_getTdsTable(OUT_CHANNEL);
			R_getDebugTable();
			R_dieuChinh100k(OUT_CHANNEL);
			R_dieuChinh1k(OUT_CHANNEL);			
			if(g_Stop == STOP_DIS)
			{
				R_luuGiaTri(OUT_CHANNEL);
				FLASH_writeParams(ADDR_FLASH_PAGE_30, g_vArr100k, g_vArr1k, 25);
				LCD_ClearScreen();
				LCD_Puts(1, 1, " PROCESS R DONE ");
			}
			else g_Stop = STOP_DIS;
			g_SelectMode = NONE;
		}

		else if(g_SelectMode == CALIB)
		{
			R_calib(OUT_CHANNEL);
			LCD_ClearScreen();
			LCD_Puts(1, 1, " CALIB DONE    ");
			g_SelectMode = NONE;
		}

		else if(g_SelectMode == TESTIO)
		{
			TESTIO_InCheck();
			TESTIO_OutCheck();
			//LCD_ClearScreen();
			//LCD_Puts(1, 1, "____IRO3_v2_____");
			//g_SelectMode = NONE;
		}

		else if(g_SelectMode == DEBUG_R)
		{
			if(g_Flag1s == true)
			{
				DEBUG_sendValueR();
				g_Flag1s = false;
			}
		}
//		HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);
//		DELAY(1000);
		#else
		uint8_t xTemp[25] = { 0x01, 0x02, 0x03, 0x04, 0x05,
											0x06, 0x07, 0x08, 0x09, 0x0a,
											0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 
											0x10, 0x11, 0x12, 0x13, 0x14, 
											0x15, 0x16, 0x17, 0x18, 0x19};
		uint8_t xTest[25];
		FLASH_write(ADDR_FLASH_PAGE_30, xTemp, sizeof(xTemp));
		FLASH_read(ADDR_FLASH_PAGE_30, xTest, sizeof(xTest));
		while(1);
		#endif
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_USART3;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
