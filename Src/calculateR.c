/*-------------------------*/
#include "calculateR.h"
#include "AD8402.h"
#include "dataProcess.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "define.h"
#include "lcd.h"

uint8_t s_tempArr100k[25], s_tempArr1k[25];
extern uint8_t g_vArr100k[25], g_vArr1k[25];
extern rxType_t g_AdcTable, g_TdsTable, g_DebugTable;
extern volatile uint8_t g_RxMode, g_FlagRx;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern uint8_t g_Stop;
extern uint8_t g_SelectMode;


void R_init(void)
{
	HAL_UART_Transmit(&huart3, (uint8_t*)"[DEBUG_EN,0]", 12, 100);
	RL1_ON;
	RL2_ON;
}

void R_getAdcTable(uint8_t channel)
{
	if(channel == IN_CHANNEL)  HAL_UART_Transmit(&huart3, (uint8_t*)"[ADC_TABLE,0]", 13, 100);
	if(channel == OUT_CHANNEL) HAL_UART_Transmit(&huart3, (uint8_t*)"[ADC_TABLE,1]", 13, 100);
	g_RxMode = ADC_MODE;
	g_FlagRx = NONE;
	while(g_FlagRx != ADC_MODE);
	g_RxMode = NONE;
	g_FlagRx = NONE;
	DATAPROCESS_tachGiatri(&g_AdcTable);
	for(uint8_t i = 2; i < 25; i++)
	g_AdcTable.VALUE[i-2] = g_AdcTable.VALUE[i];
}

void R_getTdsTable(uint8_t channel)
{
	if(channel == IN_CHANNEL)  HAL_UART_Transmit(&huart3, (uint8_t*)"[TDS_TABLE,0]", 13, 100);
	if(channel == OUT_CHANNEL) HAL_UART_Transmit(&huart3, (uint8_t*)"[TDS_TABLE,1]", 13, 100);
	g_RxMode = ADC_MODE;
	g_FlagRx = NONE;
	while(g_FlagRx != ADC_MODE);
	g_RxMode = NONE;
	g_FlagRx = NONE;
	DATAPROCESS_tachGiatri(&g_TdsTable);
	for(uint8_t i = 2; i < 25; i++)
	g_TdsTable.VALUE[i-2] = g_TdsTable.VALUE[i];
}

void R_getDebugTable(void)
{
	HAL_UART_Transmit(&huart3, (uint8_t*)"[DEBUG_EN,1]", 12, 100);
}


static bool R_kiemTraLonHon(int16_t rxAdc, int16_t vAdcTable)
{
	return ((rxAdc > vAdcTable)?true:false);
}

int16_t s_debugValue[25];
uint8_t s_pos; // luu lai vi tri ma DEBUG < ADC de calib bat dau tu phan tu tiep theo

static void R_tienXuly(uint8_t ver, uint8_t channel, int16_t *value, uint8_t pos)
{
	int16_t temp;
	while(true)
	{
		temp = *value;
		if(ver == ver100k) *value /= 2;
		if(ver == ver1k) *value -= 5;
		AD8402_writeData(ver, channel, *value);
		g_RxMode = DEBUG_MODE;			  /* enable RX IT debug */
		g_FlagRx = NONE;
		while(g_FlagRx != DEBUG_MODE);	  /* doi den khi ket thuc ban tin [a,b] lan 1*/
		g_FlagRx = NONE;
		while(g_FlagRx != DEBUG_MODE);	  /* doi den khi ket thuc ban tin [a,b] lan 2*/
		g_FlagRx = NONE;
		g_RxMode = NONE;

		DATAPROCESS_tachGiatri(&g_DebugTable);  /* tach lay du lieu gui ve tu UART */
		s_debugValue[pos] = g_DebugTable.VALUE[channel];
		if((g_AdcTable.VALUE[pos] >= g_DebugTable.VALUE[channel]) || temp == 0)
		{
			*value = temp;
			//AD8402_writeData(ver, channel, temp);
			break;
		}
	}
}


/* 20s */
void R_dieuChinh100k(uint8_t channel)
{
	LCD_ClearScreen();
	LCD_Puts(1, 1, "PROCESSING R    ");
	LCD_Puts(2, 1, "100k-step:      ");
	uint8_t pos;	
	int16_t dataTrai;
	int16_t dataPhai;
	uint8_t tempDat100k;
	int16_t data100k = 255;
	g_RxMode = NONE;
	AD8402_writeData(ver1k, channel, 255);
	AD8402_writeData(ver100k, channel, 255);
	
	/*-----------------delay de doc ve gia tri that su khi ghi max---------------------------------*/
	g_RxMode = DEBUG_MODE;            /* enable RX IT debug */
	while(g_FlagRx != DEBUG_MODE);    /* doi den khi ket thuc ban tin [a,b] lan 1*/
	g_FlagRx = NONE;
	while(g_FlagRx != DEBUG_MODE);    /* doi den khi ket thuc ban tin [a,b] lan 2*/
	g_FlagRx = NONE;
	while(g_FlagRx != DEBUG_MODE);    /* doi den khi ket thuc ban tin [a,b] lan 3*/
	g_FlagRx = NONE;
	g_RxMode = NONE;
	DATAPROCESS_tachGiatri(&g_DebugTable);  /* tach lay du lieu gui ve tu UART */
	/*---------------------------------------------------------------------------------------------*/
	
	for(pos = 0; pos < g_AdcTable.CNT-2; pos++)             
	{
		if(g_Stop == STOP_EN) break;
		char arr[16];
		sprintf(arr, "100k-step:    %2d", pos);
		LCD_Puts(2, 1, arr);
		if(R_kiemTraLonHon(g_DebugTable.VALUE[channel], g_AdcTable.VALUE[pos])) /* neu gia tri ADC trong bang > gia tri doc ve thi next den gia tri tiep theo */
		{
			R_tienXuly(ver100k, channel, &data100k, pos);
			if(data100k < 0) s_tempArr100k[pos] = 0;
			else
			{
				for(data100k = data100k; data100k >= 0; data100k--)   /* giam dan gia tri ghi cho dien tro 100k */
				{					
					if(g_Stop == STOP_EN) break;
					AD8402_writeData(ver100k, channel, data100k);
					g_RxMode = DEBUG_MODE;			  /* enable RX IT debug */
					g_FlagRx = NONE;
					while(g_FlagRx != DEBUG_MODE);	  /* doi den khi ket thuc ban tin [a,b] lan 1*/
					g_FlagRx = NONE;
					g_RxMode = NONE;
					DATAPROCESS_tachGiatri(&g_DebugTable);  /* tach lay du lieu gui ve tu UART */
					if(R_kiemTraLonHon(g_DebugTable.VALUE[channel], g_AdcTable.VALUE[pos]))  /* kiem tra gia tri ben trai > gia tri trong bang ADC */
					{						
						dataTrai = g_DebugTable.VALUE[channel];
						s_debugValue[pos] = dataTrai;				
						tempDat100k = data100k;  /* gan gia tri dang co vao bien tam */	
						s_tempArr100k[pos] = tempDat100k;
					}
					
					else
					{
						dataPhai = g_DebugTable.VALUE[channel]; /* neu gia tri gui ve tu UART < gia tri ADC thi gan vao bien tam */
						s_tempArr100k[pos] = tempDat100k; /* gan gia tri byte du lieu luc truoc cua dien tro 100k vao mang dem */	
						s_debugValue[pos] = dataTrai;
						while(dataPhai <= g_AdcTable.VALUE[pos+1])  /* neu gia tri DEBUG hien tai < gia tri ADC tiep theo thi luu gia tri byte du lieu truoc do vao vi tri tiep theo cua mang dem*/
						{
							pos++;
							if(pos == g_AdcTable.CNT) break;
							s_tempArr100k[pos] = tempDat100k;
							s_debugValue[pos] = dataTrai;					
						}
						break;
					}
				}
				if(g_Stop == STOP_EN) break;
			}
		}		
		else  s_pos++;
	}
}

/* 1p30s */
void R_dieuChinh1k(uint8_t channel)
{
	uint8_t pos;
	int16_t dataTrai, dataPhai;
	uint8_t tempDat1k;
	int16_t data1k = 255;
	g_RxMode = NONE;

//	AD8402_writeData(ver100k, channel, s_tempArr100k[2]);
//	
//	/*-----------------delay de doc ve gia tri that su khi ghi max---------------------------------*/
//	g_RxMode = DEBUG_MODE;            /* enable RX IT debug */
//	while(g_FlagRx != DEBUG_MODE);    /* doi den khi ket thuc ban tin [a,b] lan 1*/
//	g_FlagRx = NONE;
//	while(g_FlagRx != DEBUG_MODE);    /* doi den khi ket thuc ban tin [a,b] lan 2*/
//	g_FlagRx = NONE;
//	while(g_FlagRx != DEBUG_MODE);    /* doi den khi ket thuc ban tin [a,b] lan 3*/
//	g_FlagRx = NONE;
//	g_RxMode = NONE;
//	/*---------------------------------------------------------------------------------------------*/

	for(pos = s_pos; pos < g_AdcTable.CNT - 2; pos++)
	{
		if(g_Stop == STOP_EN) break;
		char arr[16];
		sprintf(arr, "1k-step:      %2d", pos);
		LCD_Puts(2, 1, arr);
		AD8402_writeData(ver100k, channel, s_tempArr100k[pos]);	
		dataTrai = s_debugValue[pos];
		if(g_AdcTable.VALUE[pos] == g_AdcTable.VALUE[pos-1]) 
		{ 
			s_tempArr1k[pos] = s_tempArr1k[pos-1]; 
			s_debugValue[pos] = s_debugValue[pos-1]; 
		}
		else
		{
			if(s_tempArr100k[pos] != s_tempArr100k[pos-1]) R_tienXuly(ver1k, channel, &data1k, pos);
			if(data1k < 0) s_tempArr1k[pos] = 0;
			else
			{			
				for(data1k = data1k; data1k >= 0; data1k--)
				{
					if(g_Stop == STOP_EN) break;
					AD8402_writeData(ver1k, channel, data1k);
					g_RxMode = DEBUG_MODE;			  /* enable RX IT debug */
					g_FlagRx = NONE;
					while(g_FlagRx != DEBUG_MODE);	  /* doi den khi ket thuc ban tin [a,b] lan 1*/
					g_FlagRx = NONE;
					g_RxMode = NONE;
					DATAPROCESS_tachGiatri(&g_DebugTable);
					if(R_kiemTraLonHon(g_DebugTable.VALUE[channel], g_AdcTable.VALUE[pos])) /* neu gia tri DEBUG > gia tri ADC thi gan gia tri DEBUG vao bien tam TRAI*/
					{
						dataTrai = g_DebugTable.VALUE[channel];
						s_debugValue[pos] = dataTrai;
						tempDat1k = data1k;  /* luu lai gia tri byte du lieu cua dien tro 1k ben trai*/
						s_tempArr1k[pos] = tempDat1k;
					}
					
					else /* neu gia tri DEBUG < gia tri ADC thi so sanh do chenh lech 2 gia tri ben trai va phai gia tri ADC */
					{
						dataPhai = g_DebugTable.VALUE[channel];
						if(R_kiemTraLonHon(g_AdcTable.VALUE[pos] - dataPhai, dataTrai - g_AdcTable.VALUE[pos]))
						{
							s_debugValue[pos] = dataTrai;
							s_tempArr1k[pos] = tempDat1k;
						}
						else 
						{
							s_debugValue[pos] = dataPhai;
							s_tempArr1k[pos] = data1k;
						}				
						if(s_tempArr100k[pos] == s_tempArr100k[pos+1]) data1k = tempDat1k;					
						else data1k = 255;					
						break;
					}
				}
				if(g_Stop == STOP_EN) break;
			}
		}
	}
	if(g_Stop == STOP_DIS) LCD_Puts(2, 1, "   done!        ");
	s_pos = 0;
}

void R_luuGiaTri(uint8_t channel)
{
	uint8_t pos;
	uint8_t x = g_AdcTable.CNT - 2;
	for(pos = 0; pos < x; pos++)
	{
		g_vArr100k[pos] = s_tempArr100k[pos];
		g_vArr1k[pos]   = s_tempArr1k[pos];
	}
}

//TDS:  {0, 9, 23,  31,  39,  50,  60,  71,  80,  91,  112, 214, 307, 400, 493, 600, 629, 629, 629, 629, 1390};
//100k: {0, 0, 45,  36,  30,  23,  19,  15,  13,  11,  9,   4,   2,   2,   1,   1,   1,   1,   1,   1,   0,}
//1k:   {0, 0, 233, 184, 211, 192, 176, 208, 209, 221, 173, 200, 244, 176, 214, 177, 170, 169, 169, 168, 159,}

void R_calib(uint8_t channel)
{
	for(uint8_t i = 0; i < g_AdcTable.CNT - 2; i++)	
	{		
		if(g_vArr100k[i] == 0 && g_vArr1k[i] == 0)
		{
		}
		else
		{
			AD8402_writeData(ver100k, channel, g_vArr100k[i]); 
			AD8402_writeData(ver1k, channel, g_vArr1k[i]); 			
			HAL_Delay(2000);				
			char sentStr[50] = "";		
			sprintf(sentStr,"[CALIB_TDS,1:%d:%d]", i, g_TdsTable.VALUE[i]);
			HAL_UART_Transmit(&huart3, (uint8_t*)sentStr, strlen(sentStr), 300);		
			HAL_Delay(200); 
		}
	}
}
