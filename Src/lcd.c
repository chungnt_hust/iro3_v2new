/* lcd.c */
#include "lcd.h"
#include "delay.h"
#include <stdlib.h>

#define GPIO_SetPinValue(GPIOx, GPIO_Pin, Val) ((Val) ? HAL_GPIO_WritePin(GPIOx, GPIO_Pin,GPIO_PIN_SET) : HAL_GPIO_WritePin(GPIOx, GPIO_Pin,GPIO_PIN_RESET))

static void LCD_EnClock(void);
static void LCD_Send4bit(uint8_t dat);
static void LCD_SendCmd(uint8_t cmd);
static void LCD_SendData(uint8_t dat);


void LCD_Init(void)
{
	DELAY(40);
	LCD_SendCmd(0x38);
	DELAY(5);
	LCD_SendCmd(0x38);
	DELAY(1);
	LCD_SendCmd(0x38);

	LCD_Send4bit(0x20);
	LCD_EnClock();
	LCD_SendCmd(0x28);
	LCD_SendCmd(0x01);
	LCD_SendCmd(0x0C);
}


void LCD_Putc(uint8_t X, uint8_t Y, char c)
{
	LCD_GotoXY(X, Y);
	LCD_SendData(c);
}

void LCD_Puts(uint8_t X, uint8_t Y, char *str)
{
	LCD_GotoXY(X, Y);
	while(*str != '\0') 
	{
		LCD_SendData(*str);
		str++;
	}
}

void LCD_PutInterger(uint8_t X, uint8_t Y, int16_t x)
{
	char arr[8];
	sprintf(arr, "%2d", x);
	LCD_Puts(X, Y, arr);
}

void LCD_GotoXY(uint8_t X, uint8_t Y)
{
	if(X == 1)  LCD_SendCmd(0x80|(Y - 1));
	if(X == 2)  LCD_SendCmd(0xC0|(Y - 1));
}

void LCD_ClearScreen()
{
	LCD_SendCmd(0x01);
	DELAY(2);
}

static void LCD_EnClock(void)
{
	GPIO_SetPinValue(GPIOB, GPIO_PIN_15, 1); // EN
	DELAY(1);
	GPIO_SetPinValue(GPIOB, GPIO_PIN_15, 0);
	DELAY(1);
}
static void LCD_Send4bit(uint8_t dat)
{
	GPIO_SetPinValue(GPIOA, GPIO_PIN_11, (dat&0x80)?1:0); // DB7
	GPIO_SetPinValue(GPIOA, GPIO_PIN_10, (dat&0x40)?1:0); // DB6
	GPIO_SetPinValue(GPIOA, GPIO_PIN_9,  (dat&0x20)?1:0); // DB5
	GPIO_SetPinValue(GPIOA, GPIO_PIN_8,  (dat&0x10)?1:0); // DB4
}

static void LCD_SendCmd(uint8_t cmd)
{
	GPIO_SetPinValue(GPIOB, GPIO_PIN_14, 0); // RS = 0
	LCD_Send4bit(cmd);
	LCD_EnClock();
	LCD_Send4bit(cmd<<4);
	LCD_EnClock();
}

static void LCD_SendData(uint8_t dat)
{
	GPIO_SetPinValue(GPIOB, GPIO_PIN_14, 1); // RS = 1
	LCD_Send4bit(dat);
	LCD_EnClock();
	LCD_Send4bit(dat<<4);
	LCD_EnClock();
}


