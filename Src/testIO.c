/*-------------------------------*/
#include "testIO.h"
#include "define.h"
#include "dataProcess.h"
#include "lcd.h"
#include <stdlib.h>

/*-------------------------------*/
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern rxType_t g_TestIo;
extern volatile uint8_t g_RxMode, g_FlagRx;
extern uint8_t g_Stop;
extern uint8_t g_SelectMode;

/*-------------------------------*/

static void TESTIO_checkAT(uint8_t status)
{
	if(status == 0)
	{
		IN_CHECK_AT_0;
		AT(1);
	}
	else
	{
		IN_CHECK_AT_1;
		AT(0);
	}
}

static void TESTIO_checkAC(uint8_t status)
{
	if(status == 0)
	{
		IN_CHECK_AC_0;
		AC(1);
	}
	
	else
	{
		IN_CHECK_AC_1;
		AC(0);
	}
}

static void TESTIO_checkDO1(uint8_t status)
{
	if(status == 0)
	{
		IN_CHECK_DO1_0;
		CS_R2(1);
	}
	
	else
	{
		IN_CHECK_DO1_1;
		CS_R2(0);
	}
}

static void TESTIO_checkVan(uint8_t status)
{
	if(status == 0)
	{
		OUT_CHECK_VAN_OFF;
	}
	
	else
	{
		OUT_CHECK_VAN_ON;
	}
}	


static void TESTIO_checkBom(uint8_t status)
{
	if(status == 0)
	{	
		OUT_CHECK_BOM_OFF;
	}
	
	else
	{
		OUT_CHECK_BOM_ON;
	}
}	
uint8_t s_numErr;
char s_AtL[16], s_AtH[16];
char s_AcL[16], s_AcH[16];
char s_Do1L[16], s_Do1H[16];
char s_VanL[16], s_VanH[16];
char s_BomL[16], s_BomH[16];

void TESTIO_InCheck(void)
{
	uint8_t cnt;
	s_numErr = 0;
	for(cnt = 0; cnt < 6; cnt++)
	{
		if(g_Stop == STOP_EN) { g_SelectMode = NONE; break; }
		g_RxMode = TESTIO_MODE;
		g_FlagRx = NONE;
		switch(cnt)
		{
			case 0:	TESTIO_checkAT(0);  LCD_Puts(1, 1, "Check AT low    "); break;
			case 1:	TESTIO_checkAT(1);  LCD_Puts(1, 1, "Check AT high   "); break;
			case 2: TESTIO_checkAC(0);  LCD_Puts(1, 1, "Check AC low    "); break;
			case 3: TESTIO_checkAC(1);  LCD_Puts(1, 1, "Check AC high   "); break;
			case 4: TESTIO_checkDO1(0); LCD_Puts(1, 1, "Check DO1 low   "); break;
			case 5: TESTIO_checkDO1(1); LCD_Puts(1, 1, "Check DO1 high  "); break;
			default: break;
		}
		while(g_FlagRx != TESTIO_MODE) if(g_Stop == STOP_EN) { g_SelectMode = NONE; break; }
		g_FlagRx = NONE;
		g_RxMode = NONE;
		DATAPROCESS_tachGiatri(&g_TestIo);
		if(g_TestIo.VALUE[1] == 0) LCD_Puts(2, 1, "result: ok!!!!!!"); 
		else 
		{ 
			LCD_Puts(2, 1, "result: false!!!");
			s_numErr++;  
			switch(cnt)
			{
				case 0: sprintf(s_AtL, "AT low false    "); break;
				case 1: sprintf(s_AtH, "AT high false   "); break;
				case 2: sprintf(s_AcL, "AC low false    "); break;
				case 3: sprintf(s_AcH, "AC high false   "); break;
				case 4: sprintf(s_Do1L, "DO1 low false   "); break;
				case 5: sprintf(s_Do1H, "DO1 high false  "); break;
			}
		}
		if(g_Stop == STOP_EN) { g_SelectMode = NONE; break; }
		else
		{
			for(uint8_t x = 0; x < 200; x++)
			{
				HAL_Delay(20);
				if(g_Stop == STOP_EN) { g_SelectMode = NONE; break; }
			}
			if(g_Stop == STOP_DIS) LCD_ClearScreen();	
		}
	}
}

void TESTIO_OutCheck(void)
{
	uint8_t cnt, status;
	for(cnt = 0; cnt < 4; cnt++)
	{
		if(g_Stop == STOP_EN) { g_SelectMode = NONE; break; }
		switch(cnt)
		{
			case 0: TESTIO_checkVan(1); status = 0; LCD_Puts(1, 1, "Check Van ON    "); break;
			case 1: TESTIO_checkVan(0); status = 1; LCD_Puts(1, 1, "Check Van OFF   "); break;
			case 2: TESTIO_checkBom(1); status = 0; LCD_Puts(1, 1, "Check Bom ON    "); break;
			case 3: TESTIO_checkBom(0); status = 1; LCD_Puts(1, 1, "Check Bom OFF   "); break;
		}
		for(uint8_t x = 0; x < 100; x++)
		{
			HAL_Delay(10);
			if(g_Stop == STOP_EN) { g_SelectMode = NONE; break; }
		}
		if(g_Stop == STOP_EN) { g_SelectMode = NONE; break; }
		if(cnt < 2) // check van
		{
			if(READ_VAN == status) LCD_Puts(2, 1, "result: ok!!!!!!");
			else 
			{ 
				s_numErr++; 
				LCD_Puts(2, 1, "result: false!!!"); 
				if(cnt == 0) sprintf(s_VanH, "Van On false    ");
				else sprintf(s_VanL, "Van Off false   ");
			}
		}

		else
		{
			if(READ_BOM == status) LCD_Puts(2, 1, "result: ok!!!!!!");
			else 
			{ 
				s_numErr++; 
				LCD_Puts(2, 1, "result: false!!!"); 
				if(cnt == 2) sprintf(s_BomH, "Bom On false    ");
				else sprintf(s_BomL, "Bom Off false   ");
			}
		}
		if(g_Stop == STOP_EN) { g_SelectMode = NONE; break; }
		else
		{
			for(uint8_t x = 0; x < 200; x++)
			{
				HAL_Delay(10);
				if(g_Stop == STOP_EN) { g_SelectMode = NONE; break; }
			}
			if(g_Stop == STOP_DIS) LCD_ClearScreen();
		}
	}
	if(g_Stop == STOP_DIS) 
	{
		LCD_Puts(1, 1, "check IO done!!!");
		for(uint8_t x = 0; x < 200; x++)
		{
			HAL_Delay(10);
			if(g_Stop == STOP_EN) { g_SelectMode = NONE; break; }
		}
	}
	else g_Stop = STOP_DIS;
}
