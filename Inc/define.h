/*-----------------------*/
#ifndef _DEFINE_H
#define _DEFINE_H

#define IN_CHANNEL 	0
#define OUT_CHANNEL 1
#define ver100k     0
#define ver1k				1


#define NONE        0
#define ADC_MODE 	  1
#define TDS_MODE 	  2
#define DEBUG_MODE  3
#define TESTIO_MODE 4

#define ENABLE 1
#define HAS    1



#define AUTO 1
#define HAND 2

#define STOP_DIS 0
#define STOP_EN  1

/* define for setup */

#define TESTIO 				 0x01
#define CALIB      			 0x02
#define PROCESS_R  			 0x03
#define DEBUG_R    	 		 0x04
#define GET_DEBUG  	 		 0x05
#define GET_ADC 			 0x06
#define GET_TDS				 0x07

#define DISPLAY_MENU		 0x09
#define SELECT_MODE 		 0x10

#define BTN_ENT     		 0x11
#define BTN_UP				 0x12
#define BTN_DOWN    		 0x13

#define NO_MODE       		 0x14
#define MODE_TEST_IO  		 0x15
#define MODE_CALIB     		 0x16
#define MODE_PROCESS_R 		 0x17
#define MODE_DEBUG    		 0x18
#define NO_MODE_ENT    		 0x19
#define MODE_TEST_IO_ENT     0x20
#define MODE_CALIB_ENT       0x21
#define MODE_PROCESS_R_ENT   0x22
#define MODE_DEBUG_ENT       0x23


#endif
