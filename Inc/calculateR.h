/*-------------------------*/
#ifndef _TINH_TOAN_R_H
#define _TINH_TOAN_R_H

#include "stm32f3xx_hal.h"


#define RL1_ON  HAL_GPIO_WritePin(RL1_GPIO_Port, RL1_Pin, GPIO_PIN_SET)
#define RL1_OFF HAL_GPIO_WritePin(RL1_GPIO_Port, RL1_Pin, GPIO_PIN_RESET)
#define RL2_ON  HAL_GPIO_WritePin(RL2_GPIO_Port, RL2_Pin, GPIO_PIN_SET)
#define RL2_OFF HAL_GPIO_WritePin(RL2_GPIO_Port, RL2_Pin, GPIO_PIN_RESET)

void R_init(void);
void R_getAdcTable(uint8_t channel);
void R_getTdsTable(uint8_t channel);
void R_getDebugTable(void);
void R_dieuChinh100k(uint8_t channel);
void R_dieuChinh1k(uint8_t channel);
void R_luuGiaTri(uint8_t channel);
void R_calib(uint8_t channel);

#endif
