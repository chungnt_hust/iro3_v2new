
#ifndef _DELAY_H
#define _DELAY_H
#include "stm32f3xx_hal.h"

void DELAY(uint16_t time);

#endif
